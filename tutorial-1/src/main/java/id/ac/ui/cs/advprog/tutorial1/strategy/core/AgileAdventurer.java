package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    @Override
    public String getAlias() {
        return "I'm a Agile Adenturer";
    }

    public AgileAdventurer() {
        AttackBehavior attack = new AttackWithGun();
        DefenseBehavior defense = new DefendWithBarrier();
        this.setAttackBehavior(attack);
        this.setDefenseBehavior(defense);
    }
}
