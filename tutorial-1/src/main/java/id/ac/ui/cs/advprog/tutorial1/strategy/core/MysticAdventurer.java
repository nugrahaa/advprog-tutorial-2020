package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    @Override
    public String getAlias() {
        return "I'm a Mystic Adventurer";
    }

    public MysticAdventurer() {
        AttackBehavior attack = new AttackWithMagic();
        DefenseBehavior defense = new DefendWithShield();
        this.setAttackBehavior(attack);
        this.setDefenseBehavior(defense);
    }
}
