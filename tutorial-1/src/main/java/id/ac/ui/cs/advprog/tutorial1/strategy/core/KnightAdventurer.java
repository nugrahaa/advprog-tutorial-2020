package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    @Override
    public String getAlias() {
        return "I'm a Knight Adventurer";
    }

    public KnightAdventurer() {
        AttackBehavior attack = new AttackWithSword();
        DefenseBehavior defense = new DefendWithArmor();
        this.setAttackBehavior(attack);
        this.setDefenseBehavior(defense);
    }
}
