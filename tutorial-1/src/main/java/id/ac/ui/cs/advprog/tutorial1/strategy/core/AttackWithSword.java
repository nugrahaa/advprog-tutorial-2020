package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    @Override
    public String attack() {
        return "I'm attacking with sword";
    }

    @Override
    public String getType() {
        return "Sword";
    }
}
