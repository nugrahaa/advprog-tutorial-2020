package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                //ToDo: Complete Me
                this.guild = guild;
        }

        @Override
        public void update() {
                String questType = this.guild.getQuestType();

                if (questType.equals("R") || questType.equals("D")) {
                        getQuests().add(this.guild.getQuest());
                }
        }

        //ToDo: Complete Me
}
