package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritAttackSpell extends HighSpiritSpell {
    public HighSpiritAttackSpell(HighSpirit highSpirit){
        super(highSpirit);
    }

    @Override
    public void cast() {
        this.spirit.attackStance();
    }

    @Override
    public String spellName() {
        return spirit.getRace() + ":Attack";
    }
}
