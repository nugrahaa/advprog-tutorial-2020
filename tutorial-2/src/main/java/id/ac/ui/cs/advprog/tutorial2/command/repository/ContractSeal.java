package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {
    private Map<String, Spell> spells;
    private Spell latestSpell = new BlankSpell();
    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        spells.get(spellName).cast();
        latestSpell = spells.get(spellName);
    }

    public void undoSpell() {
        latestSpell.undo();
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
