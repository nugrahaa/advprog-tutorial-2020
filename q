[33mcommit 9ebc2a7838469c770a3a127e6efb94b9697af0ef[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m)[m
Author: Ari Nugraha <nugrahaa878@gmail.com>
Date:   Thu Jan 30 15:20:43 2020 +0700

    Make it error

[33mcommit e712fdb9775329a540a8aa221f6d2fa401e7f913[m
Merge: d0f4617 16ce633
Author: Ari Nugraha <nugrahaa878@gmail.com>
Date:   Thu Jan 30 15:13:26 2020 +0700

    Fix conflict master and tutorial-0

[33mcommit d0f4617b537136fa581a9fd151acc5734c4e6739[m
Author: Ari Nugraha <nugrahaa878@gmail.com>
Date:   Thu Jan 30 15:06:31 2020 +0700

    Change home text in master

[33mcommit 16ce633bb062d629a8f4f8b19a042331495ebac2[m[33m ([m[1;31morigin/tutorial-0[m[33m, [m[1;32mtutorial-0[m[33m)[m
Author: Ari Nugraha <nugrahaa878@gmail.com>
Date:   Thu Jan 30 15:03:28 2020 +0700

    Change home text

[33mcommit b261a2b3b8f618c1eb424124c7c5049e08ec0dd2[m
Author: Ari Nugraha <nugrahaa878@gmail.com>
Date:   Thu Jan 30 11:55:23 2020 +0700

    Make service, controller and views for class classifier

[33mcommit 11d2ac5c10a71c909101f98d4d78c436a8340287[m[33m ([m[1;31mupstream/master[m[33m)[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Tue Jan 28 12:03:07 2020 +0700

    Update root tutorial readme missing link data
    
    Fix formatting on some missing content data.

[33mcommit b72af34c7beacd7a463c36c8b2826c6f2a570cf7[m
Merge: 7a52d6f 6d001d9
Author: Adrika Novrialdi <adrika.novrialdi@gmail.com>
Date:   Tue Jan 28 04:44:20 2020 +0000

    Merge branch 'tutorial-development' into 'master'
    
    Lab 0 Release
    
    See merge request adrika-novrialdi/advprog-tutorial-2020!2

[33mcommit 6d001d9624634ae5e1b0c834d7ccb40746a53098[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Tue Jan 28 11:37:10 2020 +0700

    Add root tutorial readme

[33mcommit 831444a17542d63e5752efc21ba062dfbd612fc0[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Tue Jan 28 00:37:19 2020 +0700

    Update tutorial 0 readme

[33mcommit f0edaeeeab59362d6222a8e3c0a75df65889cf91[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Mon Jan 27 23:35:45 2020 +0700

    Update directory and gitignore

[33mcommit fdbe880e3cc050072e6af2dfdf44eb3b20a72715[m
Author: Gagah Pangeran <gagahpangeran@gmail.com>
Date:   Mon Jan 27 15:20:52 2020 +0700

    add checklist for git tutorial

[33mcommit 3a13d03fe590160db2f637f77caf8cca7ac032a9[m
Author: Gagah Pangeran <gagahpangeran@gmail.com>
Date:   Sun Jan 26 23:38:49 2020 +0700

    finishing git tutorial

[33mcommit da646cbac1b40d3947010f1dcbe5cc1467d89a88[m
Author: Gagah Pangeran <gagahpangeran@gmail.com>
Date:   Sun Jan 26 15:18:15 2020 +0700

    add git tutorial init config commit push

[33mcommit 7a52d6f3984b95c5c0e807580c260562eed37aae[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Sat Jan 25 13:10:39 2020 +0700

    Add tutorial 0 readme

[33mcommit 832c65b3ec50ec599b90291fd2b838204e752e33[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Sat Jan 25 13:10:12 2020 +0700

    Create basic spring application for tutorial

[33mcommit 48785ab2deab9b702ae227168bed90de4e71a547[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Fri Jan 24 18:54:40 2020 +0700

    Create Tutorial 0 Application

[33mcommit 01c4ac473491c21540b71a6d69540e3002a82ce2[m
Author: adrika-novrialdi <adrika.novrialdi@gmail.com>
Date:   Fri Jan 24 16:06:44 2020 +0700

    Init AdvProg Tutorial Repository
